#!/usr/bin/python3
# -*- coding: UTF-8 -*-
"""
文件说明：RST_UART_TX.py
将需要发送的数据按照通信协议进行打包
------------------------------------------
时间            作者        版本号      记录
2021-7-6        李树益      0.1         测试版本，大致框架就是这样子了，以后再填内容
"""
import struct
from .CRC import *
"""
功能介绍：串口数据打包
"""
class UART_TX:

    def __init__(self):
        self.tx_cnt = 0

    """
    功能介绍：打包数据
    参数[msg_id]：消息类型
    参数[pdata]：需要发送的数据，格式任意
    返回[data]：打包好的数据
    """
    def Encode(self,msg_id,pdata):
        data = b''
        if msg_id == 0: #根据数据类型去找对应的打包函数
            data = self.__Data_0(pdata)

        return data

    """
    功能介绍：打包数据
    参数[pdata]：需要发送的数据，格式任意
    返回[data]：打包好的数据
    """
    def __Data_0(self,pdata):
        d0,d1,d2,d3 = pdata      #先把数据从列表中拆分出来
        data = struct.pack('!bhif',d0,d1,d2,d3)     #合成有效数据(char,short,int,float)
        tx_cnt = struct.pack('B',self.tx_cnt&0xff)  #把发送计算转换成十六进制数据
        self.tx_cnt += 1        #每发送一次信息，计数加1
        if(self.tx_cnt == 256):
            self.tx_cnt = 0
        #合成帧头
        frame_head = b'\xfa' + tx_cnt + b'\x00' + b'\x01' + b'\x01' + struct.pack('B',len(data))
        frame_head += CRC8(frame_head,len(frame_head))   #计算帧头校验
        data = frame_head + data   #合成数据包
        data += CRC16(data,len(data))   #整包校验
        return data
