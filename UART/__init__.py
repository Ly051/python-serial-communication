#!/usr/bin/python3
# -*- coding: UTF-8 -*-
"""
文件说明：__init__.py
把整个文件夹作为一个python模块。对外开放的API接口
------------------------------------------
时间            作者        版本号      记录
2021-7-6        李树益      1.0        对外开放接口完成
"""
from .UART import UART


"""
尽量少出现几千上万行的文件，不方便阅读和修改
"""

"""
命名规范：
类名全大写
函数名采用首字母大写的驼峰命名法
变量全小写，用下划线分割语义
"""

"""
对外的接口仅Send和Read，需要扩充协议内容请修改UART_TX.py和UART_RX.py
"""
