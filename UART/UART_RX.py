#!/usr/bin/python3
# -*- coding: UTF-8 -*-
"""
文件说明：RST_UART_RX.py
串口接收数据解析、储存。
------------------------------------------
时间            作者        版本号      记录
2021-7-6        李树益      0.1         测试版本，大致框架就是这样子了，以后再填内容
"""
import struct

"""
功能介绍：串口数据解析
"""
class UART_RX:

    """
    功能介绍：初始化数据集
    返回：无
    """
    def __init__(self):
        self.data = [None]*256    #最多有256种数据
        self.data[0] = [0]*4      #假设msg_id = 0的数据包含5个信息，以后确定了再一个一个写

    """
    功能介绍：解析数据
    参数[pdata]：串口接收到的一帧数据
    返回：无
    """
    def Decode(self,pdata):       #在串口接收线程里边调用
        msg_id = pdata[2]           #获取数据ID，根据数据ID进行解析
        if msg_id == 0x00:          #测试用的
            self.__Data_0(pdata)

    """
    功能介绍：解析测试数据（）msg_id = 0
    参数[pdata]：串口接收到的一帧数据
    返回：无
    """
    def __Data_0(self,pdata):
        lenght = pdata[5]
        self.data[0] = struct.unpack('!bhif',pdata[7:lenght+7])
