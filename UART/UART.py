#!/usr/bin/python3
# -*- coding: UTF-8 -*-
"""
文件说明：RST_UART.py
串口收发数据的应用层，对外开放的接口
------------------------------------------
时间            作者        版本号      记录
2021-7-6        李树益      0.1         测试版本
"""
from .UART_HAL import UART_HAL
from .UART_TX import UART_TX

"""
功能介绍：串口的应用接口，继承了UART_HAL
参数[port]：串口设备
"""
class UART(UART_HAL):
    
    def __init__(self, port='/dev/ttyUSB0'):
        super(UART,self).__init__(port=port)    #初始化父类（串口抽象层）
        self.__encode = UART_TX()

    def Send(self,msg_id,data):
        data = self.__encode.Encode(msg_id,data)
        if self.tx_fifo.full():
            try:    self.tx_fifo.get(timeout=0.05)
            except: pass
        #print(data)
        self.tx_fifo.put(data)

    def Read(self,msg_id):
        return self.decode.data[msg_id]
