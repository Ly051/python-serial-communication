# 代码说明

代码主要有两个部分，由Python实现的上位机，由RT-Thread实现的下位机。

上位机需要安装Python3和pyserial，运行test.py可以测试通信效果。

下位机需要使用RT-Thread Stdio进行编译，测试的板子是8MHz外部晶振的STM32F407VGT6开发板，需要连接串口1到PA9\PA10，连接串口2到PA2\PA3。其中串口1用作RT-Thread的控制台，串口2用来和上位机通信。

下位机的主要代码存放在applications文件夹下。

# 上位机移植

## 应用程序接口

应用程序可参考test.py文件，将UART文件夹放到工程目录下，然后引用UART，并且创建一个串口实例

```python
import UART
#创建一个串口实例，"COM3"是串口设备号
uart = UART.UART('COM3')

tx_data = (10, -1000, 65536, 0.01)	#假设我们要发送这样的数据
uart.Send(0,tx_data)	#将数据编码，放入发送线程，等待发送

#Read读取解析好的数据，参数是id，返回值是data
rx_data = uart.Read(0)             #读取接收到的0号消息
```

## 通信协议对接

### 发送

主要修改UART_TX.py文件，在 `class UART_TX:`中添加发送数据的编码函数，可参考测试用的 `def __Data_0(self,pdata):`，添加与消息ID相应的编码函数，并在 `def Encode(self,msg_id,pdata)`中根据消息的ID调用该函数，对数据进行编码。

`Encode`函数会在UART.py文件中被调用，即通过创建的串口实例调用Send函数时，会调用它进行编码，然后获取编码的数据，丢给发送线程，通过串口发送出去。

```python
#Send发送数据，参数是(id,data)
uart.Send(0,tx_data)  #0号消息用于测试，能发生5字节数据
```

### 接收

主要修改UART_RX.py文件，，在 `class UART_RX:`中的 `def __init__(self):`中给该消息的数据赋初值，并在 `class UART_RX:`中添加接收数据的解码函数，可参考测试用的 `def __Data_0(self,pdata):`，添加与消息ID相应的解码函数，并在 `def Decode(self,pdata)`中根据消息的ID调用该函数，对数据进行解码，解码出来的数据根据ID放到相应的 `self.data`中。

修改完UART_RX.py文件后，还需要修改UART_HAL.py文件，根据具体的通信协议，修改 `def __RecvLoop(self):`函数的解析过程。


## 下位机移植

下位机程序的移植和上位机的差不多，applications\uart_dma.c文件可以当作是应用demo程序，和test.py差不多。

主要的区别在于，你现在需要在applications\uart_protocol.h中添加数据的结构体，用于描述传输了哪些有用的消息。

如果你用的RTOS不是完整版的RT-Thread，那么你可能需要自己实现类似于多线程、信号量、消息队列、环形缓冲的功能。
