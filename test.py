import UART
import time
import random

#创建一个串口实例，"COM3"是串口设备号
uart = UART.UART('COM5')
tx_data = (random.randint(-128,127),random.randint(-32768,32767),random.randint(-2147483648,2147483647),random.randint(-1000,1000)/100)   #随机产生4个数字

def fun():
    
    #Send发送数据，参数是(id,data)
    uart.Send(0,tx_data)  #0号消息用于测试，能发生5字节数据

    time.sleep(0.0001)   #发送后稍微延时，可以保证数据解析完成，拿到的是刚刚发送的数据

    #Read读取解析好的数据，参数是id，返回值是data
    rx_data = uart.Read(0)             #读取接收到的0号消息

    print("tx msg 0: ",tx_data)           #输出
    print("rx msg 0: ",rx_data)           #输出(发送后立刻读取，读到的是上一帧的数据，因为发送和解析都要花时间)
    print("tx {}bytes, {}packs".format(uart.tx_bytes,uart.tx_packs))
    print("rx {}bytes, {}packs".format(uart.rx_bytes,uart.rx_packs))
    print('--------------------------------------')
    

while True:
    tx_data = (random.randint(-128,127),random.randint(-32768,32767),random.randint(-2147483648,2147483647),random.randint(-1000,1000)/100)   #随机产生4个数字
    fun()
    time.sleep(0.01)
